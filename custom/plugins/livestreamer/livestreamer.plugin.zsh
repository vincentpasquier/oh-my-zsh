alias sev="lst sevadus"
alias jdred="lst joindotared"
alias jdredh="lst joindotared high"
alias jdblue="lst joindotablue"
alias gds="lst thegdstudio"
alias gdsh="lst thegdstudio high"
alias bts="lst beyondthesummit"
alias btsh="lst beyondthesummit high"

function lst() {
	quality="best"
	if [ $# -gt 1 ]; then
		quality=$2
	fi
	nohup livestreamer twitch.tv/$1 $quality --player-passthrough hls > /dev/null &
}
